//var musicButton;
var audio = new Audio("res/music1.mp3");

//musicButton = document.getElementById("musicButton");
audio.onended = switchTrack;

function handleMusicButton() {
	/*
	musicButton = document.getElementById("musicButton");
	audio.onended = switchTrack;
	*/
	if (audio.paused) {
		audio.play();
		musicButton.innerHTML = "Musiikki: päällä";
	}
	else {
		audio.pause();
		musicButton.innerHTML = "Musiikki: pois";
	}
}

function switchTrack() {
	//alert(audio.src);
	var audioSrcArray = audio.src.split('/');
	switch (audioSrcArray[audioSrcArray.length-1]) {
		case "music1.mp3":
			audio.src = "res/music2.mp3";
			break;
		case "music2.mp3":
			audio.src = "res/music3.mp3";
			break;
		case "music3.mp3":
			audio.src = "res/music1.mp3";
			break;
	}
	audio.play();
}

function handleSoundButton() {
	if (playSounds) {
		soundButton.innerHTML = "Äänet: pois";
	}
	else {
		soundButton.innerHTML = "Äänet: päällä";
	}
	playSounds = !playSounds;
}
