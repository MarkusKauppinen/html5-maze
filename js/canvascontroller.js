// This holds the game area status
// 0 = empty
// 1 = wall
// 2 = player's surroundings
// 3 = player
// 4 = enemy
// 5 = corner
// 6 = dynamite
// 7 = a wall going down
var gameLayout = [[]];
var _gameLayout = [[]];
	
// For correctly placing the bitmaps
var mazeXOffset = 12;
var mazeYOffset = 1;
var mazeSquareSize = 15;
var dynamiteXOffset = 163;
var dynamiteYOffset = 4;
var timerXOffset = 3;
var timerYOffset = 4;
var timerWidth = 3;
var timerHeight = 109;
	
// The players coordinates
var xPlayer;
var yPlayer;

// The enemies' coordinates
var xEnemy1;
var yEnemy1;
var xEnemy2;
var yEnemy2;
var xEnemy3;
var yEnemy3;
var xEnemy4;
var yEnemy4;

// Dynamites
var dynamites = 9;
var dynamiteWaiting = false;
var dynamiteAnimation = false;
var xDynamite;
var yDynamite;

// Time left etc.
var timeLeft = 1.0;
var gameRunning = false;

// Canvas etc.
var canvas;
var ctx;
var scale = 2.0;

// Bitmaps
var player;	
var enemy;
var background;
var dynamite;
var dynamiteInMaze;
var wall;
var dynamiteFrames = [];
var wallFrames = [];
var splashScreen;

// Sounds etc.
var playSounds = false;
var moveSound = new Audio('res/move.mp3');
var cantmoveSound = new Audio('res/cantmove.mp3');
var explosionSound = new Audio('res/explosion.mp3');

function init() {
	
	// Adapt to small screens
	canvas = document.getElementById("gameCanvas");
	// 800x480 or less in portrait
	var mediaQuery = window.matchMedia( 
		"screen and (max-width: 480px) and (orientation: portrait)" );
	if( mediaQuery.matches ) {
		//alert("<= 480x800");
		// Narrow screen, adjust canvas size
		canvas.width = 0.75 * canvas.width;
		canvas.height = 0.75 * canvas.height;
		scale = 0.75 * scale;
	}
	// 800x480 or less in landscape
	var mediaQuery = window.matchMedia( 
		"screen and (max-height: 480px) and (orientation: landscape)" );
	if( mediaQuery.matches ) {
		//alert("<= 800x480");
		// Narrow screen, adjust canvas size
		canvas.width = 0.75 * canvas.width;
		canvas.height = 0.75 * canvas.height;
		scale = 0.75 * scale;
	}
	//this.resetCanvas();
	
	// Initialize the canvas and the bitmaps
	//canvas = document.getElementById("gameCanvas");
	ctx = canvas.getContext('2d');
	ctx.scale(scale, scale);
	
	background = new Image();
	background.src = "img/tausta.png";
	
	dynamite = new Image();
	dynamite.src = "img/dyna.png";
	
	dynamiteInMaze = new Image();
	dynamiteInMaze.src ="img/dyna_no_background.png";
	
	dynamiteFrames[0] = new Image();
	dynamiteFrames[0].src = "img/dyna_anim1.png";
	dynamiteFrames[1] = new Image();
	dynamiteFrames[1].src = "img/dyna_anim2.png";
	dynamiteFrames[2] = new Image();
	dynamiteFrames[2].src = "img/dyna_anim3.png";
	dynamiteFrames[3] = new Image();
	dynamiteFrames[3].src = "img/dyna_anim4.png";
	dynamiteFrames[4] = new Image();
	dynamiteFrames[4].src = "img/dyna_anim5.png";
	dynamiteFrames[5] = new Image();
	dynamiteFrames[5].src = "img/dyna_anim6.png";
	dynamiteFrames[6] = new Image();
	dynamiteFrames[6].src = "img/dyna_anim7.png";
	dynamiteFrames[7] = new Image();
	dynamiteFrames[7].src = "img/dyna_anim8.png";
	dynamiteFrames[8] = new Image();
	dynamiteFrames[8].src = "img/dyna_anim9.png";
	
	wallFrames[0] = new Image();
	wallFrames[0].src = "img/este_anim1.png";
	wallFrames[1] = new Image();
	wallFrames[1].src = "img/este_anim2.png";
	wallFrames[2] = new Image();
	wallFrames[2].src = "img/este_anim3.png";
	wallFrames[3] = new Image();
	wallFrames[3].src = "img/este_anim4.png";
	wallFrames[4] = new Image();
	wallFrames[4].src = "img/este_anim5.png";
	
	player = new Image();
	player.src = "img/omaHahmo.png";
	
	enemy = new Image();
	enemy.src = "img/vartija.png";
	
	wall = new Image();
	wall.src = "img/este.png";
	
	splashScreen = new Image();
	splashScreen.onload = function() {
		ctx.drawImage(splashScreen, 0, 0);  
	};
	splashScreen.src ="img/splash_screen.png";
	
	// Listen to mouse/finger events
	canvas.onmousedown = handleMouseDown;
	canvas.touchstart = handleMouseDown;
	
	// Listen to arrow keys
	canvas.addEventListener("keyup", handleKeyUp, true);
	
	// Prevent scrolling with arrow keys when game running
	// http://stackoverflow.com/questions/8916620/disable-arrow-key-scrolling-in-users-browser
	window.addEventListener("keydown", function(e) {
    	// space and arrow keys
    	if([32, 37, 38, 39, 40].indexOf(e.keyCode) > -1) {
        	e.preventDefault();
    	}
	}, false);
			
}

function isMovingNextToEnemy(direction) {
	
	// Skip this check to make the game easier
	return false;
/*	
	var _xPlayer = xPlayer;
	var _yPlayer = yPlayer;
	
	// Simulate the move
	if (direction == "up") _yPlayer--;
	if (direction == "down") _yPlayer++;
	if (direction == "left") _xPlayer--;
	if (direction == "right") _xPlayer++;
	
	// Enemy above?
	if (_yPlayer > 0 && gameLayout[_xPlayer][_yPlayer-1] == 4) return true;
	// Enemy below?
	if (_yPlayer < 8 && gameLayout[_xPlayer][_yPlayer+1] == 4) return true;
	// Enemy on left?
	if (_xPlayer > 1 && gameLayout[_xPlayer-1][_yPlayer] == 4) return true;
	// Enemy on right?
	if (_xPlayer < 9 && gameLayout[_xPlayer+1][_yPlayer] == 4) return true;
		
	// Okay to make the move
	return false;
*/
}

function calculateEnemyMove(xEnemy, yEnemy) {
	// TODO: Needs some serious refactoring to avoid pretty much the same code
	// being repeated again and again
	
	// Is the horizontal distance to player higher than the vertical?
	if (Math.abs(xEnemy - xPlayer) > Math.abs(yEnemy - yPlayer)) {
		// Yes, try to move horizontally
		// Which direction?
		if (xEnemy < xPlayer) {
			// Right, can we make the move?
			if (xEnemy < 9 && gameLayout[xEnemy+1][yEnemy] == 0) {
				// Yes
				xEnemy++;
			} 
			else {
				// No, try to move vertically instead
				// Which direction?
				if (yEnemy < yPlayer) {
					// Down, can we make the move?
					if (yEnemy < 8 && gameLayout[xEnemy][yEnemy + 1] == 0) {
						// Yes
						yEnemy++;
					}
					else {
						// No, give up and wait for the next turn	
					}
				} 
				else {
					// Up, can we make the move?
					if (yEnemy > 0 && gameLayout[xEnemy][yEnemy - 1] == 0) {
						// Yes
						yEnemy--;
					}
					else {
						// No, give up and wait for the next turn	
					}
				}
			}
		} 
		else {
			// Left, can we make the move?
			if (xEnemy > 0 && gameLayout[xEnemy-1][yEnemy] == 0) {
				// Yes
				xEnemy--;
			} 
			else {
				// No, try to move vertically
				// Which way?
				if (yEnemy < yPlayer) {
					// Down, can we make the move?
					if (yEnemy < 8 && gameLayout[xEnemy][yEnemy + 1] == 0) {
						yEnemy++;
					}
					else {
						// No, give up and wait for the next turn	
					}
				} 
				else {
					// Up, can we make the move?
					if (yEnemy > 0 && gameLayout[xEnemy][yEnemy - 1] == 0) {
						yEnemy--;
					}
					else {
						// No, give up and wait for the next turn	
					}
				}
			}
		}
	} 
	else {
		// No, try to move vertically
		// Which way?
		if (yEnemy < yPlayer) {
			// Down, can we make the move?
			if (yEnemy < 8 && gameLayout[xEnemy][yEnemy + 1] == 0) {
				// Yes
				yEnemy++;
			} 
			else {
				// No, try to move horizontally instead
				// Which way?
				if (xEnemy < xPlayer) {
					// Right, can we make the move?
					if (xEnemy < 9 && gameLayout[xEnemy+1][yEnemy] == 0) {
						// Yes
						xEnemy++;
					}
					else {
						// No, give up and wait for the next turn	
					}
				} else {
					// Left, can we make the move?
					if (xEnemy > 0 && gameLayout[xEnemy-1][yEnemy] == 0) {
						// Yes
						xEnemy--;
					}
					else {
						// No, give up and wait for the next turn	
					}
				}
			}
		} 
		else {
			// Up, can we make the move?
			if (yEnemy > 0 && gameLayout[xEnemy][yEnemy - 1] == 0) {
				// Yes
				yEnemy--;
			} 
			else {
				// No, try to move horizontally instead
				// Which way?
				if (xEnemy < xPlayer) {
					// Right, can we make the move?
					if (xEnemy < 9 && gameLayout[xEnemy+1][yEnemy] == 0) {
						// Yes
						xEnemy++;
					}
					else {
						// No, give up and wait for the next turn	
					}
				} else {
					// Left, can we make the move?
					if (xEnemy > 0 && gameLayout[xEnemy-1][yEnemy] == 0) {
						// Yes
						xEnemy--;
					}
					else {
						// No, give up and wait for the next turn	
					}
				}
			}
		}
	}
	return [xEnemy, yEnemy];
}

function moveEnemies() {
	
	gameLayout[xEnemy1][yEnemy1] = 0;
	var newPos = calculateEnemyMove(xEnemy1, yEnemy1);
	xEnemy1 = newPos[0];
	yEnemy1 = newPos[1];
	gameLayout[xEnemy1][yEnemy1] = 4;
	
	gameLayout[xEnemy2][yEnemy2] = 0;
	newPos = calculateEnemyMove(xEnemy2, yEnemy2);
	xEnemy2 = newPos[0];
	yEnemy2 = newPos[1];
	gameLayout[xEnemy2][yEnemy2] = 4;
	
	gameLayout[xEnemy3][yEnemy3] = 0;
	newPos = calculateEnemyMove(xEnemy3, yEnemy3);
	xEnemy3 = newPos[0];
	yEnemy3 = newPos[1];
	gameLayout[xEnemy3][yEnemy3] = 4;
	
	gameLayout[xEnemy4][yEnemy4] = 0;
	newPos = calculateEnemyMove(xEnemy4, yEnemy4);
	xEnemy4 = newPos[0];
	yEnemy4 = newPos[1];
	gameLayout[xEnemy4][yEnemy4] = 4;
}

function startGame() {
	
	if (gameRunning) return;
	
	dynamites = 9;
	timeLeft = 1.0;
	
	// Initialize the 2D arrays
	// TODO: Use a loop
	gameLayout[0] = [0,0,0,0,0,0,0,0,0];
	gameLayout[1] = [0,0,0,0,0,0,0,0,0];
	gameLayout[2] = [0,0,0,0,0,0,0,0,0];
	gameLayout[3] = [0,0,0,0,0,0,0,0,0];
	gameLayout[4] = [0,0,0,0,0,0,0,0,0];
	gameLayout[5] = [0,0,0,0,0,0,0,0,0];
	gameLayout[6] = [0,0,0,0,0,0,0,0,0];
	gameLayout[7] = [0,0,0,0,0,0,0,0,0];
	gameLayout[8] = [0,0,0,0,0,0,0,0,0];
	gameLayout[9] = [0,0,0,0,0,0,0,0,0];
	
	_gameLayout[0] = [0,0,0,0,0,0,0,0,0];
	_gameLayout[1] = [0,0,0,0,0,0,0,0,0];
	_gameLayout[2] = [0,0,0,0,0,0,0,0,0];
	_gameLayout[3] = [0,0,0,0,0,0,0,0,0];
	_gameLayout[4] = [0,0,0,0,0,0,0,0,0];
	_gameLayout[5] = [0,0,0,0,0,0,0,0,0];
	_gameLayout[6] = [0,0,0,0,0,0,0,0,0];
	_gameLayout[7] = [0,0,0,0,0,0,0,0,0];
	_gameLayout[8] = [0,0,0,0,0,0,0,0,0];
	_gameLayout[9] = [0,0,0,0,0,0,0,0,0];
	
	// Populate the gameLayout 2D array with data
	
	// Mark the corners
	gameLayout[0][0] = 5;
	gameLayout[9][0] = 5;
	gameLayout[0][8] = 5;
	gameLayout[9][8] = 5;
	
	// Place some walls close to the corners to avoid too easy of a game
	gameLayout[1][1] = 1;
	gameLayout[8][1] = 1;
	gameLayout[8][7] = 1;
	gameLayout[1][7] = 1;

	// Place 35 more wall pieces at random locations
	var count = 0;
	while (count < 35) {
		var x = Math.floor(Math.random()*10);
		var y = Math.floor(Math.random()*9);
		
		if (gameLayout[x][y] == 0) {
			// Mark the square
			gameLayout[x][y] = 1;
			count++;
		}
	}

	// Place the player at a random location but not too close to the edges
	// of the maze
	count = 0;
	while (count < 1) {
		var x = Math.floor(Math.random()*6) + 2;
		var y = Math.floor(Math.random()*5) + 2;
		
		if (gameLayout[x][y] == 0) {
			// Set these coordinates for the player
			gameLayout[x][y] = 3;
			// Also mark the surrounding squares to avoid placing an enemy right to the next square
			gameLayout[x-1][y - 1] = 2;
			gameLayout[x-1][y] = 2;
			gameLayout[x-1][y + 1] = 2;
			gameLayout[x][y - 1] = 2;
			gameLayout[x][y + 1] = 2;
			gameLayout[x+1][y - 1] = 2;
			gameLayout[x+1][y] = 2;
			gameLayout[x+1][y + 1] = 2;

			xPlayer = x;
			yPlayer = y;

			count++;
		}
	}
	
	// TODO: Refactor, same code is repeated many times
	// Place enemy #1
	count = 0;
	while (count < 1) {
		var x = Math.floor(Math.random()*7) + 1;
		var y = Math.floor(Math.random()*6) + 1;
		
		if (gameLayout[x][y] == 0) {
			gameLayout[x][y] = 4;
			xEnemy1 = x;
			yEnemy1 = y;
			count++;
		}
	}
	// Place enemy #2
	count = 0;
	while (count < 1) {
		var x = Math.floor(Math.random()*7) + 1;
		var y = Math.floor(Math.random()*6) + 1;
		
		if (gameLayout[x][y] == 0) {
			gameLayout[x][y] = 4;
			xEnemy2 = x;
			yEnemy2 = y;
			count++;
		}
	}
	// Place enemy #3
	count = 0;
	while (count < 1) {
		var x = Math.floor(Math.random()*7) + 1;
		var y = Math.floor(Math.random()*6) + 1;
		
		if (gameLayout[x][y] == 0) {
			gameLayout[x][y] = 4;
			xEnemy3 = x;
			yEnemy3 = y;
			count++;
		}
	}
	// Place enemy #4
	count = 0;
	while (count < 1) {
		var x = Math.floor(Math.random()*7) + 1;
		var y = Math.floor(Math.random()*6) + 1;
		
		if (gameLayout[x][y] == 0) {
			gameLayout[x][y] = 4;
			xEnemy4 = x;
			yEnemy4 = y;
			count++;
		}
	}
	
	// Clear the surrounding squares of the player before the game begins
	gameLayout[xPlayer-1][yPlayer - 1] = 0;
	gameLayout[xPlayer-1][yPlayer] = 0;
	gameLayout[xPlayer-1][yPlayer + 1] = 0;
	gameLayout[xPlayer][yPlayer - 1] = 0;
	gameLayout[xPlayer][yPlayer + 1] = 0;
	gameLayout[xPlayer+1][yPlayer - 1] = 0;
	gameLayout[xPlayer+1][yPlayer] = 0;
	gameLayout[xPlayer+1][yPlayer + 1] = 0;
	
	// Draw the bitmaps (game background, player, enemies, dynamites)
	//drawBitmaps();
	window.requestAnimationFrame(drawBitmaps);
	
	// Draw the timer bar
	drawTimer();
	
	gameRunning = true;
	reduceTime();	
	
	canvas.focus();
}

function endGame(victory) {
	gameRunning = false;
	
	// Update graphics
	//drawBitmaps();
	window.requestAnimationFrame(drawBitmaps);
	
	// The alert may interfere with graphics update, add a delay.
	// Only add a delay when animating an explosion
	var delay;
	if (dynamiteAnimation) {
		delay = 1500;
	}
	else {
		delay = 0;
	}
	
	setTimeout(
		function() {
			if (victory) {
				alert("Voitit! :)");
			}
			else {
				alert("Hävisit! :(");
			}			
		}, delay);
}

function move(direction) {
	var moveMade = false;
	
	if (!gameRunning) return;
	
	// Remember dynamite position
	xDynamite = xPlayer;
	yDynamite = yPlayer;
	
	// Remember the game layout apart from the player's position.
	// This is needed for animations.
	// TODO: Use push(), slice() or some other "proper" way?
	var i, j;
	for ( i = 0; i < 10; i++) {
		for ( j = 0; j < 9; j++) {
			_gameLayout[i][j] = gameLayout[i][j];
		}
	}
	
	// TODO: Could use some refactoring
	if (direction == "up") {
		//alert(gameLayout[xPlayer][yPlayer-2]);
		if (yPlayer > 0 &&
			!isMovingNextToEnemy(direction) && 
			(gameLayout[xPlayer][yPlayer-1] == 0 ||
			gameLayout[xPlayer][yPlayer-1] == 2 ||
			gameLayout[xPlayer][yPlayer-1] == 5)) {
			// Move
			yPlayer--;
			gameLayout[xPlayer][yPlayer+1] = 0;
			moveMade = true;
		}
	}
	else if (direction == "down") {
		if (yPlayer < 8 && 
			!isMovingNextToEnemy(direction) &&
			(gameLayout[xPlayer][yPlayer+1] == 0 ||
			gameLayout[xPlayer][yPlayer+1] == 2 ||
			gameLayout[xPlayer][yPlayer+1] == 5)) {
			// Move
			yPlayer++;
			gameLayout[xPlayer][yPlayer-1] = 0;
			moveMade = true;
		}
	}
	else if (direction == "left") {
		if (xPlayer > 0 && 
			!isMovingNextToEnemy(direction) &&
			(gameLayout[xPlayer-1][yPlayer] == 0 ||
			gameLayout[xPlayer-1][yPlayer] == 2 ||
			gameLayout[xPlayer-1][yPlayer] == 5)) {
			// Move
			xPlayer--;
			gameLayout[xPlayer+1][yPlayer] = 0;
			moveMade = true;
		}
	}
	else if (direction == "right") {
		if (xPlayer < 9 && 
			!isMovingNextToEnemy(direction) &&
			(gameLayout[xPlayer+1][yPlayer] == 0 ||
			gameLayout[xPlayer+1][yPlayer] == 2 ||
			gameLayout[xPlayer+1][yPlayer] == 5)) {
			// Move
			xPlayer++;
			gameLayout[xPlayer-1][yPlayer] = 0;
			moveMade = true;
		}
	}

	if (moveMade) {
		if (playSounds) moveSound.play();
		
		// Did the player make it to a corner?
		if (gameLayout[xPlayer][yPlayer] == 5) {
			// End the game
			endGame(true);	
		}
		
		// Update game status
		gameLayout[xPlayer][yPlayer] = 3;
		_gameLayout[xPlayer][yPlayer] = 3;
		
		// Blow up some stuff?
		if (dynamiteWaiting) {
			
			// Show an animation
			dynamiteAnimation = true;
			
			// Remove any walls on top, bottom, left or right
			if (xDynamite+1 <= 9 && gameLayout[xDynamite+1][yDynamite] == 1) gameLayout[xDynamite+1][yDynamite] = 7;
			if (xDynamite-1 >= 0 && gameLayout[xDynamite-1][yDynamite] == 1) gameLayout[xDynamite-1][yDynamite] = 7;
			if (yDynamite+1 <= 8 && gameLayout[xDynamite][yDynamite+1] == 1) gameLayout[xDynamite][yDynamite+1] = 7;
			if (yDynamite-1 >= 0 && gameLayout[xDynamite][yDynamite-1] == 1) gameLayout[xDynamite][yDynamite-1] = 7;
			
			// The player was erased, fix it
			gameLayout[xPlayer][yPlayer] = 3;
			
			dynamiteWaiting = false;
		}
		
		// Calculate the enemy moves
		moveEnemies();
		// Draw again
		window.requestAnimationFrame(drawBitmaps);	
		
		// Are there still possible moves?
		if ((yPlayer == 0 || gameLayout[xPlayer][yPlayer-1] == 1 || gameLayout[xPlayer][yPlayer-1] == 4) &&
			(yPlayer == 8 || gameLayout[xPlayer][yPlayer+1] == 1 || gameLayout[xPlayer][yPlayer+1] == 4) &&
			(xPlayer == 0 || gameLayout[xPlayer-1][yPlayer] == 1 || gameLayout[xPlayer-1][yPlayer] == 4) &&
			(xPlayer == 9 || gameLayout[xPlayer+1][yPlayer] == 1 || gameLayout[xPlayer+1][yPlayer] == 4)) {
			// No, end the game, player lost
			if (gameRunning) endGame(false);
		}
	}
	else {
		if (playSounds) cantmoveSound.play();
	}
}

function handleKeyUp(e) {
	// Don't move during the explosion
	if (dynamiteAnimation == false) {
		switch (e.keyCode) {
		case 38:
			move("up");
			break;
		case 40:
			move("down");
			break;
		case 37:
			move("left");
			break;
		case 39:
			move("right");
			break;
		case 32:
			// Dynamite
			if(dynamites > 0 && !dynamiteWaiting && !dynamiteAnimation) {
				gameLayout[xPlayer][yPlayer] = 6;
				dynamiteWaiting = true;
				dynamites--;
				ctx.drawImage(dynamiteInMaze, mazeXOffset + xPlayer * mazeSquareSize, mazeYOffset + yPlayer * mazeSquareSize);
			}
			break;
		}
	}
	return false;
}

function handleMouseDown(e) {
	// Position inside the canvas in pizels
	var boundingRect = canvas.getBoundingClientRect();
	var canvasX = e.clientX - boundingRect.left;
	var canvasY = e.clientY - boundingRect.top;

	// Position inside the maze as columns, rows
	var mazeWidth = boundingRect.width - mazeXOffset - dynamiteXOffset;
	var mazeHeight = boundingRect.height - mazeYOffset - mazeYOffset;
	var posX = parseInt((canvasX - scale * mazeXOffset) / (scale * mazeSquareSize));
	var posY = parseInt((canvasY - scale * mazeYOffset) / (scale * mazeSquareSize));
	
	// If the click happened on the dynamites then prepare to blast away a wall
	if(posX == 10 && dynamites > 0 && !dynamiteWaiting && !dynamiteAnimation) {
		gameLayout[xPlayer][yPlayer] = 6;
		dynamiteWaiting = true;
		dynamites--;
		ctx.drawImage(dynamiteInMaze, mazeXOffset + xPlayer * mazeSquareSize, mazeYOffset + yPlayer * mazeSquareSize);
		// Skip the rest of this function
		return;
	}

	// Don't move during the explosion
	if (dynamiteAnimation == false) {
		// Which way to move? Diagonal moves not allowed
		if (posX < xPlayer && posY == yPlayer) {
			// Left
			move("left");
		} else if (posX > xPlayer && posY == yPlayer) {
			// Right
			move("right");
		} else if (posY < yPlayer && posX == xPlayer) {
			// Up
			move("up");
		} else if (posY > yPlayer && posX == xPlayer) {
			// Down
			move("down");
		} else {
			// No action
		}
	}
}

function drawBitmaps() {

	// Draw the background
	ctx.drawImage(background, 0, 0);
	drawTimer();
	
	if (dynamiteAnimation && gameRunning) {
		// Iterate the old 2D array and draw the bitmaps based on the contents
		var i, j;
		for ( i = 0; i < 10; i++) {
			for ( j = 0; j < 9; j++) {
				if (_gameLayout[i][j] == 1 || _gameLayout[i][j] == 7) {
					ctx.drawImage(wall, mazeXOffset + i * mazeSquareSize, mazeYOffset + j * mazeSquareSize);
				} else if (_gameLayout[i][j] == 3) {
					ctx.drawImage(player, mazeXOffset + i * mazeSquareSize, mazeYOffset + j * mazeSquareSize);
				} else if (_gameLayout[i][j] == 4) {
					ctx.drawImage(enemy, mazeXOffset + i * mazeSquareSize, mazeYOffset + j * mazeSquareSize);
				}
			}
		}
	}
	else {
		// Iterate the new 2D array and draw the bitmaps based on the contents
		var i, j;
		for ( i = 0; i < 10; i++) {
			for ( j = 0; j < 9; j++) {
				if (gameLayout[i][j] == 1 || gameLayout[i][j] == 7) {
					ctx.drawImage(wall, mazeXOffset + i * mazeSquareSize, mazeYOffset + j * mazeSquareSize);
				} else if (gameLayout[i][j] == 3) {
					ctx.drawImage(player, mazeXOffset + i * mazeSquareSize, mazeYOffset + j * mazeSquareSize);
				} else if (gameLayout[i][j] == 4) {
					ctx.drawImage(enemy, mazeXOffset + i * mazeSquareSize, mazeYOffset + j * mazeSquareSize);
					
				}
			}
		}
	}
	
	// Draw the dynamites
	for(i=0; i<dynamites; i++) {
		ctx.drawImage(dynamite, dynamiteXOffset, dynamiteYOffset + i * mazeSquareSize);
	}
	
	// Animate the explosion
	if (dynamiteAnimation && gameRunning) {
		
		// TODO: Use for or for-each(?)
		// Dynamite
		ctx.drawImage(dynamiteFrames[0], mazeXOffset + xDynamite * mazeSquareSize, mazeYOffset + yDynamite * mazeSquareSize);
		setTimeout(function() {
				ctx.drawImage(dynamiteFrames[0], mazeXOffset + xDynamite * mazeSquareSize, mazeYOffset + yDynamite * mazeSquareSize);},100);
		setTimeout(function() {
				ctx.drawImage(dynamiteFrames[1], mazeXOffset + xDynamite * mazeSquareSize, mazeYOffset + yDynamite * mazeSquareSize);},200);
		setTimeout(function() {
				ctx.drawImage(dynamiteFrames[2], mazeXOffset + xDynamite * mazeSquareSize, mazeYOffset + yDynamite * mazeSquareSize);},300);
		setTimeout(function() {
				ctx.drawImage(dynamiteFrames[3], mazeXOffset + xDynamite * mazeSquareSize, mazeYOffset + yDynamite * mazeSquareSize);},400);
		setTimeout(function() {
				ctx.drawImage(dynamiteFrames[4], mazeXOffset + xDynamite * mazeSquareSize, mazeYOffset + yDynamite * mazeSquareSize);},500);
		setTimeout(function() {
				ctx.drawImage(dynamiteFrames[5], mazeXOffset + xDynamite * mazeSquareSize, mazeYOffset + yDynamite * mazeSquareSize);},600);
		setTimeout(function() {
				ctx.drawImage(dynamiteFrames[6], mazeXOffset + xDynamite * mazeSquareSize, mazeYOffset + yDynamite * mazeSquareSize);},700);
		setTimeout(function() {
				ctx.drawImage(dynamiteFrames[7], mazeXOffset + xDynamite * mazeSquareSize, mazeYOffset + yDynamite * mazeSquareSize);},800);
		setTimeout(function() {
				ctx.drawImage(dynamiteFrames[8], mazeXOffset + xDynamite * mazeSquareSize, mazeYOffset + yDynamite * mazeSquareSize);},900);
		
		// Draws everything immediately
		/*
		var frame;
		for (frame = 0; frame < dynamiteFrames.length-1; frame++) {
			setTimeout(function() {
				ctx.drawImage(
					dynamiteFrames[frame], 
					mazeXOffset + xDynamite * mazeSquareSize, 
					mazeYOffset + yDynamite * mazeSquareSize);}, 
					frame * 100);
		}		
		*/
		
		// Explosion sound
		setTimeout(function() {
			if (playSounds) explosionSound.play();},900);
		
		// Walls
		// Right from dynamite
		if (xDynamite < 9 && gameLayout[xDynamite+1][yDynamite] == 7) {
			setTimeout(function() {
				ctx.drawImage(wallFrames[0], mazeXOffset + (xDynamite+1) * mazeSquareSize, mazeYOffset + yDynamite * mazeSquareSize);},800);
			setTimeout(function() {
				ctx.drawImage(wallFrames[1], mazeXOffset + (xDynamite+1) * mazeSquareSize, mazeYOffset + yDynamite * mazeSquareSize);},900);
			setTimeout(function() {
				ctx.drawImage(wallFrames[2], mazeXOffset + (xDynamite+1) * mazeSquareSize, mazeYOffset + yDynamite * mazeSquareSize);},1000);
			setTimeout(function() {
				ctx.drawImage(wallFrames[3], mazeXOffset + (xDynamite+1) * mazeSquareSize, mazeYOffset + yDynamite * mazeSquareSize);},1100);
			setTimeout(function() {
				ctx.drawImage(wallFrames[4], mazeXOffset + (xDynamite+1) * mazeSquareSize, mazeYOffset + yDynamite * mazeSquareSize);},1200);	
		}
		// Left from dynamite
		if (xDynamite > 0 && gameLayout[xDynamite-1][yDynamite] == 7) {
			setTimeout(function() {
				ctx.drawImage(wallFrames[0], mazeXOffset + (xDynamite-1) * mazeSquareSize, mazeYOffset + yDynamite * mazeSquareSize);},800);
			setTimeout(function() {
				ctx.drawImage(wallFrames[1], mazeXOffset + (xDynamite-1) * mazeSquareSize, mazeYOffset + yDynamite * mazeSquareSize);},900);
			setTimeout(function() {
				ctx.drawImage(wallFrames[2], mazeXOffset + (xDynamite-1) * mazeSquareSize, mazeYOffset + yDynamite * mazeSquareSize);},1000);
			setTimeout(function() {
				ctx.drawImage(wallFrames[3], mazeXOffset + (xDynamite-1) * mazeSquareSize, mazeYOffset + yDynamite * mazeSquareSize);},1100);
			setTimeout(function() {
				ctx.drawImage(wallFrames[4], mazeXOffset + (xDynamite-1) * mazeSquareSize, mazeYOffset + yDynamite * mazeSquareSize);},1200);
		}
		// Down from dynamite
		if (yDynamite < 8 && gameLayout[xDynamite][yDynamite+1] == 7) {
			setTimeout(function() {
				ctx.drawImage(wallFrames[0], mazeXOffset + xDynamite * mazeSquareSize, mazeYOffset + (yDynamite+1) * mazeSquareSize);},800);
			setTimeout(function() {
				ctx.drawImage(wallFrames[1], mazeXOffset + xDynamite * mazeSquareSize, mazeYOffset + (yDynamite+1) * mazeSquareSize);},900);
			setTimeout(function() {
				ctx.drawImage(wallFrames[2], mazeXOffset + xDynamite * mazeSquareSize, mazeYOffset + (yDynamite+1) * mazeSquareSize);},1000);
			setTimeout(function() {
				ctx.drawImage(wallFrames[3], mazeXOffset + xDynamite * mazeSquareSize, mazeYOffset + (yDynamite+1) * mazeSquareSize);},1100);
			setTimeout(function() {
				ctx.drawImage(wallFrames[4], mazeXOffset + xDynamite * mazeSquareSize, mazeYOffset + (yDynamite+1) * mazeSquareSize);},1200);
		}
		// Up from dynamite
		if (yDynamite > 0 && gameLayout[xDynamite][yDynamite-1] == 7) {
			setTimeout(function() {
				ctx.drawImage(wallFrames[0], mazeXOffset + xDynamite * mazeSquareSize, mazeYOffset + (yDynamite-1) * mazeSquareSize);},800);
			setTimeout(function() {
				ctx.drawImage(wallFrames[1], mazeXOffset + xDynamite * mazeSquareSize, mazeYOffset + (yDynamite-1) * mazeSquareSize);},900);
			setTimeout(function() {
				ctx.drawImage(wallFrames[2], mazeXOffset + xDynamite * mazeSquareSize, mazeYOffset + (yDynamite-1) * mazeSquareSize);},1000);
			setTimeout(function() {
				ctx.drawImage(wallFrames[3], mazeXOffset + xDynamite * mazeSquareSize, mazeYOffset + (yDynamite-1) * mazeSquareSize);},1100);
			setTimeout(function() {
				ctx.drawImage(wallFrames[4], mazeXOffset + xDynamite * mazeSquareSize, mazeYOffset + (yDynamite-1) * mazeSquareSize);},1200);
		}
		
		setTimeout(
			function() {
				// Set the blown up walls as clear space
				var i, j;
				for ( i = 0; i < 10; i++) {
					for ( j = 0; j < 9; j++) {
						if (gameLayout[i][j] == 7) gameLayout[i][j] = 0;
					}
				}
				// This is needed for some pixel level 
				// cleanup when the graphics are scaled up. 
				dynamiteAnimation = false;
				window.requestAnimationFrame(drawBitmaps);
				},1300);
	}
}

function drawTimer() {
	ctx.beginPath();
	ctx.rect(timerXOffset, timerYOffset, timerXOffset + timerWidth, timerYOffset + timerHeight);
    ctx.fillStyle = "gray";
    ctx.fill();
	
	ctx.beginPath();
    ctx.rect(timerXOffset, timerYOffset + (1 - timeLeft) * timerHeight, timerXOffset + timerWidth, timerYOffset + timeLeft * timerHeight);
    ctx.fillStyle = "silver";
    ctx.fill();
}

function reduceTime() {
	// TODO: Could a web worker be used?
	if (gameRunning && timeLeft > 0) {
		setTimeout(
			function() {
				timeLeft -= 0.01666666;
				drawTimer();
				reduceTime();	
			}
		,1000);
	}
	else if (gameRunning) {
		endGame(false);
	}
}
